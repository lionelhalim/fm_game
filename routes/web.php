<?php

use App\Http\Controllers\Answer\Difference\AnswerDifferenceController;
use App\Http\Controllers\Answer\Power\AnswerPowerController;
use App\Http\Controllers\Crypt\EncryptionController;
use App\Http\Controllers\Event\EventHistoryController;
use App\Http\Controllers\Leaderboard\LeaderBoardController;
use App\Http\Controllers\RegisterController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('encrypt/{payload}', [EncryptionController::class, 'encrypt']);
Route::get('decrypt/{payload}', [EncryptionController::class, 'decrypt']);

Route::prefix('register')->group(function () {
    //abort(404);
    Route::get('/', [RegisterController::class, 'index'])->name('index.register');
    Route::post('/', [RegisterController::class, 'register'])->name('register');
});

Route::prefix('event')->group(function () {
    //abort(404);
    route::get('history', [EventHistoryController::class, 'history'])->name('event.history');
});

Route::prefix('difference')->group(function () {
    Route::prefix('answer')->group(function () {
        Route::get('form/{encryptedObjectId}', [AnswerDifferenceController::class, 'index'])->name('index.difference.answer');
        Route::post('{encryptedObjectId}', [AnswerDifferenceController::class, 'answer'])->name('difference.answer');
    });
});

Route::prefix('power_up')->group(function () {
    Route::prefix('answer')->group(function () {
        Route::get('form/{encryptedObjectId}', [AnswerPowerController::class, 'index'])->name('index.power.answer');
        Route::post('{encryptedObjectId}', [AnswerPowerController::class, 'answer'])->name('power_up.answer');
    });
});

Route::get('leaderboard', [LeaderBoardController::class, 'index']);

Route::get('hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/jawabannya/hidden/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm', function () {
    return view('question.path1');
});
