<?php

use App\Helper\Power\PowerList;

return [
    'correct' => [
        'object_diff' => 'Berhasil menemukan object yang berbeda. Mendapatkan :SCORE skor',
        'power_up' => 'Keyword benar! Berhasil mendapatkan power up'
    ],

    'wrong' => [
        'object_diff' => 'Object yang ditebak tidak mengalami perubahan. Mengalami pengurangan skor sebanyak :SCORE',
        'power_up' => 'Keyword salah! Gagal mendapatkan power up'
    ],

    'use' => [
        'power_up' => [
            PowerList::POWER_DOUBLE_SCORE => 'blablablobloblo! point yang didapatkan dikali 2. Skor yang didapatkan menjadi :SCORE',
            PowerList::POWER_NO_PENALTY => 'humbahumba! tidak ada pengurangan point'
        ],
    ]
];
