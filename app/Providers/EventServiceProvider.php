<?php

namespace App\Providers;

use App\Events\Correct;
use App\Events\DifferenceCorrectListener;
use App\Events\DifferenceWrongListener;
use App\Events\Wrong;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        Correct::class => [
            DifferenceCorrectListener::class
        ],

        Wrong::class => [
            DifferenceWrongListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
