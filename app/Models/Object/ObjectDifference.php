<?php

namespace App\Models\Object;

use App\Models\Team\Team;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ObjectDifference extends Model
{
    use HasFactory;

    protected $table = 'object__difference';
    public $timestamps = false;

    protected $fillable = [
        'object_type', 'object_name', 'status', 'score', 'founder'
    ];

    public function object(): BelongsTo
    {
        return $this->belongsTo(ObjectModel::class, 'object_type', 'object_type');
    }

    public function team(): BelongsTo
    {
        return $this->belongsTo(Team::class, 'founder', 'team_name');
    }
}
