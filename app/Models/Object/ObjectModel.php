<?php

namespace App\Models\Object;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ObjectModel extends Model
{
    use HasFactory;

    protected $table = 'object';
    public $timestamps = false;

    protected $fillable = [
        'object_type', 'description'
    ];

    public function differences(): HasMany
    {
        return $this->hasMany(ObjectDifference::class, 'object_type', 'object_type');
    }

    public function powerUps(): HasMany
    {
        return $this->hasMany(ObjectPowerUp::class, 'object_type', 'object_type');
    }
}
