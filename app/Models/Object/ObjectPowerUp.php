<?php

namespace App\Models\Object;

use App\Models\Power\PowerUpModel;
use App\Models\Team\Team;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ObjectPowerUp extends Model
{
    use HasFactory;

    protected $table = 'object__power_up';
    public $timestamps = false;

    protected $fillable = [
        'object_type', 'object_name', 'power_name', 'question', 'keyword', 'status', 'amount', 'founder'
    ];

    public function object(): BelongsTo
    {
        return $this->belongsTo(ObjectModel::class, 'object_type', 'object_type');
    }

    public function power(): BelongsTo
    {
        return $this->belongsTo(PowerUpModel::class, 'power_name', 'power_name');
    }

    public function team(): BelongsTo
    {
        return $this->belongsTo(Team::class, 'founder', 'team_name');
    }
}
