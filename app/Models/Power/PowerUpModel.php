<?php

namespace App\Models\Power;

use App\Models\Object\ObjectPowerUp;
use App\Models\Team\TeamPowerUp;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class PowerUpModel extends Model
{
    use HasFactory;

    protected $table = 'power_up';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = [
        'power_name', 'description'
    ];

    public function objects(): hasMany
    {
        return $this->hasMany(ObjectPowerUp::class, 'power_name', 'power_name');
    }

    public function owners(): HasMany
    {
        return $this->hasMany(TeamPowerUp::class, 'power_name', 'power_name');
    }
}
