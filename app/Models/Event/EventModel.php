<?php

namespace App\Models\Event;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class EventModel extends Model
{
    use HasFactory;

    protected $table = 'event';
    protected $primaryKey = 'event_name';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'event_name', 'description'
    ];

    public function history(): HasMany
    {
        return $this->hasMany(EventHistory::class, 'event_name', 'event_name');
    }
}
