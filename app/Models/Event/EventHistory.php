<?php

namespace App\Models\Event;

use App\Models\Team\Team;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class EventHistory extends Model
{
    use HasFactory;

    protected $table = 'event__history';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'event_name', 'team_name', 'event', 'created_at', 'updated_at'
    ];

    public function event(): BelongsTo
    {
        $this->belongsTo(EventModel::class, 'event_name', 'event_name');
    }

    public function team(): BelongsTo
    {
        $this->belongsTo(Team::class, 'team_name', 'team_name');
    }
}
