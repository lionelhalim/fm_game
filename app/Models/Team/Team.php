<?php

namespace App\Models\Team;

use App\Models\Event\EventHistory;
use App\Models\Object\ObjectModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Team extends Model
{
    use HasFactory;

    protected $table = 'team';
    protected $primaryKey = 'team_name';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'team_name', 'password'
    ];

    public function powerUps(): HasMany
    {
        return $this->hasMany(TeamPowerUp::class, 'team_name', 'team_name');
    }

    public function objects(): HasMany
    {
        return $this->hasMany(ObjectModel::class, 'founder', 'team_name');
    }

    public function statistic(): HasOne
    {
        return $this->hasOne(TeamStatistic::class, 'team_name', 'team_name');
    }

    public function eventHistory(): HasMany
    {
        return $this->hasMany(EventHistory::class, 'team_name', 'team_name');
    }
}
