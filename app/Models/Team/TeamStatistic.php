<?php

namespace App\Models\Team;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TeamStatistic extends Model
{
    use HasFactory;

    protected $table = 'team__statistic';
    protected $primaryKey = 'team_name';
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable = [
        'team_name', 'score'
    ];

    public function team(): BelongsTo
    {
        return $this->belongsTo(Team::class, 'team_name', 'team_name');
    }
}
