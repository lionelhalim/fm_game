<?php

namespace App\Models\Team;

use App\Models\Power\PowerUpModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TeamPowerUp extends Model
{
    use HasFactory;

    protected $table = 'team__power_up';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = [
        'power_up_id', 'team_name', 'amount'
    ];

    public function power(): BelongsTo
    {
        $this->belongsTo(PowerUpModel::class, 'power_name', 'power_name');
    }

    public function team(): BelongsTo
    {
        $this->belongsTo(Team::class, 'team_name', 'team_name');
    }
}
