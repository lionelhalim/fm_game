<?php

namespace App\Http\Controllers\Leaderboard;

use App\Http\Controllers\Controller;
use App\Models\Event\EventHistory;
use App\Models\Team\TeamStatistic;
use Illuminate\Http\Request;

class LeaderBoardController extends Controller
{
    public function index()
    {
        $teamStatistics = TeamStatistic::query()
            ->orderBy('score', 'desc')
            ->orderBy('updated_at', 'asc')
            ->get();

        $eventHistory = EventHistory::query()
            ->orderBy('id', 'desc')
            ->limit(12)
            ->get();

        return view('leaderboard.index', [
            'teamStatistics' => $teamStatistics,
            'eventHistory' => $eventHistory
        ]);
    }
}
