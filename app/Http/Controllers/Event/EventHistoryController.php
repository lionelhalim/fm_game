<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Models\Event\EventHistory;
use Illuminate\Http\Request;

class EventHistoryController extends Controller
{
    public function __construct()
    {
        abort(404);
    }

    public function history()
    {
        dd(EventHistory::query()->get());
    }
}
