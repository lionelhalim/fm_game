<?php

namespace App\Http\Controllers\Crypt;

use App\Helper\Crypt\CryptHelper;
use App\Http\Controllers\Controller;

class EncryptionController extends Controller
{
    public function __construct()
    {
        abort(404);
    }

    public function encrypt($payload): string
    {
        return CryptHelper::encryptString($payload);
    }

    public function decrypt($payload)
    {
        return CryptHelper::decryptString($payload);
    }
}
