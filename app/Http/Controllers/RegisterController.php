<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Services\RegisterService;

class RegisterController
{
    public function __construct()
    {
        abort(404);
    }

    public function index()
    {
        return view('register');
    }

    public function register(RegisterRequest $request)
    {
        $registerService = new RegisterService($request->team_name, $request->password);
        $registerService->handle();

        return response('success');
    }
}
