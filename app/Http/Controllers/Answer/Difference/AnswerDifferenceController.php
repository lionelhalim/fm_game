<?php

namespace App\Http\Controllers\Answer\Difference;

use App\Helper\Object\ObjectList;
use App\Helper\Team\TeamCredHelper;
use App\Http\Controllers\Answer\BaseAnswerObjectController;
use App\Http\Requests\Answer\AnswerObjectRequest;
use App\Models\Team\Team;
use App\Services\Object\ObjectDifferenceService;

class AnswerDifferenceController extends BaseAnswerObjectController
{
    public const ENC_LIST = [
            'eyJpdiI6Ijh5TmdEZEs2bEVSZXYxbmxhNVZkYnc9PSIsInZhbHVlIjoiYnNzV2JRYTczVUhLNEtMdFFKSkVadz09IiwibWFjIjoiMGVlN2ExMmNiYWI0NGQyNDY0YWRkZTJkZGI3ODlhNzg4Njk2YjhmZDUwYTQ1NDg2MGQ0NTExZDhjMDFlNmNkMCIsInRhZyI6IiJ9:QJmrXl2JcK',
            'eyJpdiI6IjIvMXV4Z0MvTUljTHZGMWdsS1hIMmc9PSIsInZhbHVlIjoiZmQ4aE10R2FpOE5uYkVQTlZseFRoQT09IiwibWFjIjoiNzlmOWYyNjgxNzZlMWVmODZhZGJkNjBmOTdjYjJkM2NmYTI3NzYxNjQ1NjRlNWNlN2JiNmM3YWY5MTU0MjBiMCIsInRhZyI6IiJ9:QJmrXl2JcK',
            'eyJpdiI6ImVRUGdXblQ0YlZUUGVIYTNYZVVVVUE9PSIsInZhbHVlIjoiQXpYclBWTnl6dFZHdmU5RzhqRGp3UT09IiwibWFjIjoiYThjNTEyZTk3OTEzYjg4MjBlNjg5NjgyNTk1NTAwZTFlM2VkMTg2YmVhNmUzYTRjMWM0MThkODMwOWQzNDkzNyIsInRhZyI6IiJ9:QJmrXl2JcK',
            'eyJpdiI6IjkzZHczNS8rcnJxSHJ1eW5QRU8rcWc9PSIsInZhbHVlIjoiSVJ3cHhObFRxMkRrZWZlR25MRWV4QT09IiwibWFjIjoiOTliMjE4ZTdkYTk5NGIwYmNiNzgzMTY3YjE0NjI2MWRlOTg3YTViMmYwYmQ1MzQzODAxMmM1MmQ5NDA3MTVmMiIsInRhZyI6IiJ9:QJmrXl2JcK',
            'eyJpdiI6InMxcmFWWjNRYTRWZmJPd1FXMzdSc0E9PSIsInZhbHVlIjoiS2NHMjZveFZiZzQ1L2cxeFVqT0FPUT09IiwibWFjIjoiYzQyYmNmMjQ0N2FlM2E1MDBlZGUyMGUzMWQyZTM4YTc0YTIyYTI2YjYzZjE1MTdmOGEwZmQwNDhiYWU4MGE4MiIsInRhZyI6IiJ9:QJmrXl2JcK',
            'eyJpdiI6ImNJZW5FeTFqVzZpTzJEZVlUR3lFTVE9PSIsInZhbHVlIjoieG1FRnVRbnFlb3U1VE9zWkFDclkvdz09IiwibWFjIjoiNzdhMDNmOWE2ZDExNTg2M2EyYWRjZTgyNGNlNDYxOTM4MDM5ZmMxNDM0NmM5MWRiNjRmNDcxZmMzODc5ZGRiOSIsInRhZyI6IiJ9:QJmrXl2JcK',
            'eyJpdiI6IkdaZDdReDBzUXo4cE1xNEFaOG4rM1E9PSIsInZhbHVlIjoiTlBlN1NUTWZhRXpsWXpiY3lnM29DZz09IiwibWFjIjoiMjczMGYwNDhlY2JlZDE4N2RhNmZjYTk0ZWEwZTAwY2UwMDgxMTU5YmFiODY5Zjc3Yzg0OTVkOTk0MjQ4Y2NhMyIsInRhZyI6IiJ9:QJmrXl2JcK',
            'eyJpdiI6IldPQ2tad0UvZFR5ZFpDYndhUnNhRVE9PSIsInZhbHVlIjoiOVEzTFF4NEM4ZGNpNWk3bzRYWEFyQT09IiwibWFjIjoiNjc2OTBlMzAyNjcxYWM3YjI3OTFmYWM3OTdlNjExOTFmMGZjMzg1ZGMxMDU1MTdlOWYxYTU0MzJlMTAzNmRjNiIsInRhZyI6IiJ9:QJmrXl2JcK',
            'eyJpdiI6IkZXY1ViY096eE9YK014clBXL2Zwa0E9PSIsInZhbHVlIjoiUlhoZytReWxobHdqUG5YUW1qVjI1dz09IiwibWFjIjoiMjM5NTgyZGNhMjFkMmU3NTE4ODkxMzg0Yjc3MjUxYTRkNzFjZjY2NGZlYTE0YTE2OWU0N2VlNzVlM2I2ZDlhMyIsInRhZyI6IiJ9:QJmrXl2JcK',
            'eyJpdiI6Im00dWt6WXlFR2diZWpZYWsrWGxUekE9PSIsInZhbHVlIjoiTkRhUEpva3I5c3I1TlVvSnJqMTliQT09IiwibWFjIjoiMjQyMjYwMWFkOTA3NzBkZjM5ZDhiOGIwZGFkMGRlMGVhN2JhNThjYzU2NTk5NTliOTg4ZmIwNzI3NjFlYWE2NCIsInRhZyI6IiJ9:QJmrXl2JcK',
        ];

    public function __construct()
    {
        parent::__construct(ObjectList::OBJECT_DIFFERENCE);
    }

    public function index(string $encryptedObjectId)
    {
        $objectId = $this->getObjectID($encryptedObjectId);
        $this->validateObjectID($objectId);

        $objectService = new ObjectDifferenceService($objectId);

        if ($objectService->isGuessed()) {
            return $this->getObjectGuessedResponse($objectService);
        } else {
            return view('difference.answer', [
                'objectId' => $encryptedObjectId,
                'teams' => Team::query()->select('team_name')->get(),
            ]);
        }
    }

    public function answer(AnswerObjectRequest $request, string $encryptedObjectId)
    {
        $objectId = $this->getObjectID($encryptedObjectId);
        $this->validateObjectID($objectId);

        $team = TeamCredHelper::getTeam($request->team, $request->password);

        if (isset($team)) {
            $objectService = new ObjectDifferenceService($objectId);

            if ($objectService->isGuessed()) {
                return $this->getObjectGuessedResponse($objectService);
            } else {
                if ($objectService->object) {
                    $response = $objectService->handleCorrect($team);
                } else {
                    $response = $objectService->handleWrong($team);
                }
            }
        } else {
            $response = [
                'status' => 0,
                'message' => 'Password salah',
                'data' => null
            ];
        }

        return view('difference.result', ['response' => $response]);
    }
}
