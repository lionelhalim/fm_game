<?php

namespace App\Http\Controllers\Answer\Power;

use App\Helper\Object\ObjectList;
use App\Helper\Team\TeamCredHelper;
use App\Http\Controllers\Answer\BaseAnswerObjectController;
use App\Http\Requests\Answer\AnswerPowerUpRequest;
use App\Models\Team\Team;
use App\Services\Object\ObjectPowerUpService;

class AnswerPowerController extends BaseAnswerObjectController
{
    public const ENC_LIST = [
            'eyJpdiI6Im9WNWJ1RXRPa0xGN0tPaFVOcWZHR2c9PSIsInZhbHVlIjoidy81b2tkWFRZdm0zNnk4Z2VndWdOZz09IiwibWFjIjoiMDE1YTE2MGJmZjdhNmU3MWJjZjI0MzExYmNiMjc0OGFjMGRkMzhkNjYyYzUxMmVlMDY1ZDQxNjIxMTAyZDI4MiIsInRhZyI6IiJ9:QJmrXl2JcK',
            'eyJpdiI6IkRXNkZpQmxSTzlTNHNJYUFNS095RFE9PSIsInZhbHVlIjoiKytxNUd3elZmRFF3UTZxTzFUdDFkdz09IiwibWFjIjoiOWVhMzU1ZDhjMzkwMGFiYjVjMzIzYzI2ZWRiZTA3NzZjNDUyM2RhNzc5N2QwNzkzNzQ2M2Q4MjE3NTg2OTAwNyIsInRhZyI6IiJ9:QJmrXl2JcK',
            'eyJpdiI6ImZSQnc2Q0V4N0NSay9CNmlIblpOb3c9PSIsInZhbHVlIjoiTkswcGJ3TlQ0TzRzSmI4d2ZETlZpQT09IiwibWFjIjoiZTc2MjQ2OTE2Njc1MDQxYTY1MWEwZTA2YzljZGQyNGM0ODBiZmEzNjJlZGEwYjZmMzdkMTQ4OTJmMDMxZjBkNiIsInRhZyI6IiJ9:QJmrXl2JcK',
            'eyJpdiI6ImwwRTZNMjJ4d2ZlRGtvVU1YMkRtNHc9PSIsInZhbHVlIjoiV21oVHRtcFlkWEt4ZkRmUFdqL3NBdz09IiwibWFjIjoiOTJjNWFhOGY0ZmZkM2QwYmExZDRkYjAzMDgxMzAwYmY2MWY1NDNhZjg4OTA3NDcwYmNlYmY0NjllYTNlYWMwMCIsInRhZyI6IiJ9:QJmrXl2JcK',
        ];

    public function __construct()
    {
        parent::__construct(ObjectList::OBJECT_POWER_UP);
    }

    public function index(string $encryptedObjectId)
    {
        $objectId = $this->getObjectID($encryptedObjectId);
        $this->validateObjectID($objectId);

        $objectService = new ObjectPowerUpService($objectId);

        if (is_null($objectService->object)) {
            abort(404);
        } else {
            if ($objectService->isGuessed()) {
                return $this->getObjectGuessedResponse($objectService);
            } else {
                return view('power_up.answer', [
                    'objectId' => $encryptedObjectId,
                    'teams' => Team::query()->select('team_name')->get(),
                    'question' => $objectService->object->question
                ]);
            }
        }
    }

    public function answer(AnswerPowerUpRequest $request, string $encryptedObjectId)
    {
        $objectId = $this->getObjectID($encryptedObjectId);
        $this->validateObjectID($objectId);

        $keyword = strtolower($request->keyword);
        $team = TeamCredHelper::getTeam($request->team, $request->password);

        if (isset($team)) {
            $objectService = new ObjectPowerUpService($objectId);

            if ($objectService->isGuessed()) {
                return $this->getObjectGuessedResponse($objectService);
            } else {
                if ($objectService->object && $objectService->isRightKeyword($keyword)) {
                    $response = $objectService->handleCorrect($team);
                } else {
                    $response = $objectService->handleWrong($team);
                }
            }
        } else {
            $response = [
                'status' => 0,
                'message' => 'Password salah',
                'data' => null
            ];
        }

        return view('power_up.result', ['response' => $response]);
    }
}
