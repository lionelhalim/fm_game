<?php

namespace App\Http\Controllers\Answer;

use App\Helper\Crypt\CryptHelper;
use App\Helper\Object\ObjectList;
use App\Http\Controllers\Answer\Difference\AnswerDifferenceController;
use App\Http\Controllers\Answer\Power\AnswerPowerController;
use App\Http\Controllers\Controller;
use App\Services\Object\ObjectDifferenceService;
use App\Services\Object\ObjectPowerUpService;

class BaseAnswerObjectController extends Controller
{
    protected $objectType;

    public function __construct(string $objectType)
    {
        $this->objectType = $objectType;
    }

    /**
     * @param string $encryptedObjectId
     * @return mixed
     */
    protected function getObjectID(string $encryptedObjectId)
    {
        try {
            if ($this->objectType == ObjectList::OBJECT_DIFFERENCE) {
                if (in_array($encryptedObjectId, AnswerPowerController::ENC_LIST)) {
                    throw new \Exception('nonono');
                }
            } elseif ($this->objectType == ObjectList::OBJECT_POWER_UP) {
                if (in_array($encryptedObjectId, AnswerDifferenceController::ENC_LIST)) {
                    throw new \Exception('nonono');
                }
            }

            return CryptHelper::decryptString($encryptedObjectId);
        } catch (\Exception $e) {
            echo 'Jangan diganti-ganti ya urlnya';
            abort(404);
        }

        return null;
    }

    protected function validateObjectID($objectId): void
    {
        if (!preg_match('/^[1-9][0-9]*$/', $objectId)) {
            abort(500, 'ehe!');
        }
    }

    /**
     * @param ObjectDifferenceService | ObjectPowerUpService $objectService
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    protected function getObjectGuessedResponse($objectService)
    {
        $response = [
            'status' => 0,
            'message' => 'Sudah ditebak oleh tim ' . $objectService->object->team->team_name,
            'data' => null
        ];

        return view("$this->objectType.result", ['response' => $response]);
    }
}
