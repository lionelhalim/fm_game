<?php

namespace App\Helper\Event;

class EventList
{
    public const EVENT_CORRECT = 'correct';
    public const EVENT_WRONG = 'wrong';

    public const EVENTS = [
        [
            'event_name' => self::EVENT_CORRECT,
            'description' => 'Positive Event',
        ],
        [
            'event_name' => self::EVENT_WRONG,
            'description' => 'Negative Event',
        ],
    ];
}
