<?php

namespace App\Helper\Crypt;

use Illuminate\Support\Facades\Crypt;

class CryptHelper
{
    const PEPPER = ':QJmrXl2JcK';

    public static function encryptString(string $payload): string
    {
        return Crypt::encrypt($payload) . self::PEPPER;
    }

    public static function decryptString(string $payload)
    {
        return Crypt::decrypt(self::removePepperFromPayload($payload));
    }

    public static function removePepperFromPayload(string $payload): string
    {
        return str_replace(self::PEPPER, '', $payload);
    }
}
