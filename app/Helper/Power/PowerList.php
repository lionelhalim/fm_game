<?php

namespace App\Helper\Power;

class PowerList
{
    public const POWER_DOUBLE_SCORE = 'Double Score';
    public const POWER_NO_PENALTY = 'No Penalty';

    public const POWERS = [
        [
            'power_name' => self::POWER_DOUBLE_SCORE,
            'description' => 'Multiple gained score by 2x',
        ],
        [
            'power_name' => self::POWER_NO_PENALTY,
            'description' => 'Nullify point penalty',
        ],
    ];
}
