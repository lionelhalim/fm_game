<?php

namespace App\Helper\Object;

use App\Helper\Power\PowerList;

class ObjectList
{
    public const OBJECT_DIFFERENCE = 'difference';
    public const OBJECT_POWER_UP = 'power_up';

    public const OBJECTS = [
        [
            'object_type' => self::OBJECT_DIFFERENCE,
            'description' => 'Difference object to be guessed',
        ],
        [
            'object_type' => self::OBJECT_POWER_UP,
            'description' => 'Power up object',
        ],
    ];

    public const OBJECT_DIFFERENCE_LIST = [
        [
            'object_type' => self::OBJECT_DIFFERENCE,
            'object_name' => 'object1',
            'score' => 3,
        ],
        [
            'object_type' => self::OBJECT_DIFFERENCE,
            'object_name' => 'object2',
            'score' => 3,
        ],
        [
            'object_type' => self::OBJECT_DIFFERENCE,
            'object_name' => 'object3',
            'score' => 3,
        ],
        [
            'object_type' => self::OBJECT_DIFFERENCE,
            'object_name' => 'object4',
            'score' => 3,
        ],
        [
            'object_type' => self::OBJECT_DIFFERENCE,
            'object_name' => 'object5',
            'score' => 3,
        ],
        [
            'object_type' => self::OBJECT_DIFFERENCE,
            'object_name' => 'object6',
            'score' => 3,
        ],
        [
            'object_type' => self::OBJECT_DIFFERENCE,
            'object_name' => 'object7',
            'score' => 3,
        ],
        [
            'object_type' => self::OBJECT_DIFFERENCE,
            'object_name' => 'object8',
            'score' => 3,
        ],
        [
            'object_type' => self::OBJECT_DIFFERENCE,
            'object_name' => 'object9',
            'score' => 3,
        ],
        [
            'object_type' => self::OBJECT_DIFFERENCE,
            'object_name' => 'object10',
            'score' => 3,
        ],
    ];

    public const OBJECT_POWER_UP_DOUBLE_SCORE = [
        [
            'object_type' => self::OBJECT_POWER_UP,
            'object_name' => 'doublescore1',
            'power_name' => PowerList::POWER_DOUBLE_SCORE,
            'question' => 'Hari ini hari apa?',
            'keyword' => 'jumat',
            'amount' => 1,
        ],
        [
            'object_type' => self::OBJECT_POWER_UP,
            'object_name' => 'doublescore2',
            'power_name' => PowerList::POWER_DOUBLE_SCORE,
            'question' => 'พรุ่งนี้วันอะไร',
            'keyword' => 'sabtu',
            'amount' => 1,
        ],
    ];

    public const OBJECT_POWER_UP_NO_PENALTY = [
        [
            'object_type' => self::OBJECT_POWER_UP,
            'object_name' => 'nopenalty1',
            'power_name' => PowerList::POWER_NO_PENALTY,
            'question' => 'Pertanyaannya ada di mana ya?',
            'keyword' => 'kamis',
            'amount' => 1,
        ],
        [
            'object_type' => self::OBJECT_POWER_UP,
            'object_name' => 'nopenalty2',
            'power_name' => PowerList::POWER_NO_PENALTY,
            'question' => 'https://cms.iak.dev/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/jawabannya/hidden/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm/hmmm',
            'keyword' => 'hidden',
            'amount' => 1,
        ],
    ] ;
}
