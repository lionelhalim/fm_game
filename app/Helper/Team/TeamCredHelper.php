<?php

namespace App\Helper\Team;

use App\Models\Team\Team;
use Illuminate\Database\Eloquent\Model;

class TeamCredHelper
{
    /**
     * @param $teamName
     * @param $password
     * @return Team|Model|null
     */
    public static function getTeam($teamName, $password): ?Team
    {
        return Team::query()
            ->with('statistic')
            ->where('team_name', $teamName)
            ->where('password', $password)->first();
    }
}
