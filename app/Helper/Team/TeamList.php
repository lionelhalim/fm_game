<?php

namespace App\Helper\Team;

class TeamList
{
    const TEAM_MONYET = 'Monyet';
    const TEAM_HARIMAU = 'Harimau';
    const TEAM_NAGA = 'Naga';
    const TEAM_KELINCI = 'Kelinci';
    const TEAM_TIKUS = 'Tikus';
    const TEAM_ULAR = 'Ular';
    const TEAM_KUDA = 'Kuda';

    public const TEAMS = [
        [
            'team_name' => self::TEAM_MONYET,
            'password' => '0vY3VbPnmB',
        ],
        [
            'team_name' => self::TEAM_HARIMAU,
            'password' => 'mEdWcsTYhJ',
        ],
        [
            'team_name' => self::TEAM_NAGA,
            'password' => 'wl7Tk7d5CH',
        ],
        [
            'team_name' => self::TEAM_KELINCI,
            'password' => 'HB1xdYZf94',
        ],
        [
            'team_name' => self::TEAM_TIKUS,
            'password' => 'KfckNNjaC7',
        ],
        [
            'team_name' => self::TEAM_ULAR,
            'password' => 'IMCxkNjjUC',
        ],
        [
            'team_name' => self::TEAM_KUDA,
            'password' => 'C5SuMFWA01',
        ],
    ];

    public const TEAM_MEMBERS_MONYET = [
        [
            'team_name' => self::TEAM_MONYET,
            'member_name' => 'Reza',
        ],
        [
            'team_name' => self::TEAM_MONYET,
            'member_name' => 'Fachri',
        ],
        [
            'team_name' => self::TEAM_MONYET,
            'member_name' => 'Syahrul',
        ],
        [
            'team_name' => self::TEAM_MONYET,
            'member_name' => 'Benny',
        ],
        [
            'team_name' => self::TEAM_MONYET,
            'member_name' => 'Jansen',
        ],
        [
            'team_name' => self::TEAM_MONYET,
            'member_name' => 'Sherina',
        ],
    ];

    public const TEAM_MEMBERS_HARIMAU = [
        [
            'team_name' => self::TEAM_HARIMAU,
            'member_name' => 'Lionel',
        ],
        [
            'team_name' => self::TEAM_HARIMAU,
            'member_name' => 'Jessica',
        ],
        [
            'team_name' => self::TEAM_HARIMAU,
            'member_name' => 'Calvin',
        ],
        [
            'team_name' => self::TEAM_HARIMAU,
            'member_name' => 'Shendy',
        ],
        [
            'team_name' => self::TEAM_HARIMAU,
            'member_name' => 'Kevin',
        ],
        [
            'team_name' => self::TEAM_HARIMAU,
            'member_name' => 'Elin',
        ],
    ];

    public const TEAM_MEMBERS_NAGA = [
        [
            'team_name' => self::TEAM_NAGA,
            'member_name' => 'Gandhi',
        ],
        [
            'team_name' => self::TEAM_NAGA,
            'member_name' => 'Devi',
        ],
        [
            'team_name' => self::TEAM_NAGA,
            'member_name' => 'Stainley',
        ],
        [
            'team_name' => self::TEAM_NAGA,
            'member_name' => 'Ando',
        ],
        [
            'team_name' => self::TEAM_NAGA,
            'member_name' => 'Wilson',
        ],
    ];

    public const TEAM_MEMBERS_KELINCI = [
        [
            'team_name' => self::TEAM_KELINCI,
            'member_name' => 'Richard',
        ],
        [
            'team_name' => self::TEAM_KELINCI,
            'member_name' => 'Cunde',
        ],
        [
            'team_name' => self::TEAM_KELINCI,
            'member_name' => 'Stephanie',
        ],
        [
            'team_name' => self::TEAM_KELINCI,
            'member_name' => 'Rico',
        ],
        [
            'team_name' => self::TEAM_KELINCI,
            'member_name' => 'Nielsen',
        ],
        [
            'team_name' => self::TEAM_KELINCI,
            'member_name' => 'Hanna',
        ],
    ];

    public const TEAM_MEMBERS_TIKUS = [
        [
            'team_name' => self::TEAM_TIKUS,
            'member_name' => 'Aseng',
        ],
        [
            'team_name' => self::TEAM_TIKUS,
            'member_name' => 'Juan',
        ],
        [
            'team_name' => self::TEAM_TIKUS,
            'member_name' => 'Metta',
        ],
        [
            'team_name' => self::TEAM_TIKUS,
            'member_name' => 'Reza',
        ],
        [
            'team_name' => self::TEAM_TIKUS,
            'member_name' => 'Nutri',
        ],
    ];

    public const TEAM_MEMBERS_ULAR = [
        [
            'team_name' => self::TEAM_TIKUS,
            'member_name' => 'Anis',
        ],
        [
            'team_name' => self::TEAM_TIKUS,
            'member_name' => 'Lia',
        ],
        [
            'team_name' => self::TEAM_TIKUS,
            'member_name' => 'Eddy',
        ],
        [
            'team_name' => self::TEAM_TIKUS,
            'member_name' => 'Maria',
        ],
        [
            'team_name' => self::TEAM_TIKUS,
            'member_name' => 'Yuyun',
        ],
    ];

    public const TEAM_MEMBERS_KUDA = [
        [
            'team_name' => self::TEAM_KUDA,
            'member_name' => 'Firza',
        ],
        [
            'team_name' => self::TEAM_KUDA,
            'member_name' => 'Teguh',
        ],
        [
            'team_name' => self::TEAM_KUDA,
            'member_name' => 'Ilyas',
        ],
        [
            'team_name' => self::TEAM_KUDA,
            'member_name' => 'Bayu',
        ],
        [
            'team_name' => self::TEAM_KUDA,
            'member_name' => 'Ian',
        ],
        [
            'team_name' => self::TEAM_KUDA,
            'member_name' => 'Ajo',
        ],
        [
            'team_name' => self::TEAM_KUDA,
            'member_name' => 'Sheila',
        ],
    ];
}
