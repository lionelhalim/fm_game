<?php

namespace App\Services\Object;

use App\Helper\Event\EventList;
use App\Models\Event\EventHistory;
use App\Models\Object\ObjectPowerUp;
use App\Models\Team\Team;
use App\Models\Team\TeamPowerUp;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ObjectPowerUpService extends BaseObjectService
{
    /**
     * @var int $amount
     */
    protected $score;

    public function __construct($objectId)
    {
        parent::__construct($objectId, new ObjectPowerUp());
    }

    public function isRightKeyword($keyword)
    {
        return $this->object->keyword == $keyword;
    }

    public function handleCorrect(Team $team): array
    {
        try {
            DB::beginTransaction();

            $team = $this->updateTeamPowerUp($team);
            $this->markAsGuessed($team);
            $this->logEvent($team, true);
            $response = [
                'status' => 1,
                'message' => "Berhasil mendapatkan {$this->object->power_name}",
                'data' => [
                    'amount' => $this->object->amount,
                    'founder' => $team->team_name,
                    'total_amount' => $this->getTeamPowerUp($team)->amount,
                ]
            ];

            DB::commit();
        } catch (\Exception $e) {
            Log::error($e->getMessage(), [$e]);
            DB::rollBack();
            abort(500);
        }

        return $response;
    }

    public function handleWrong(Team $team): array
    {
        $this->logEvent($team, false);

        return [
            'status' => 0,
            'message' => 'Keyword salah',
            'data' => null,
        ];
    }

    private function updateTeamPowerUp(Team $team): Team
    {
        $amount = $this->object->amount;
        $this->getTeamPowerUp($team)->increment('amount', $amount);

        return $team->refresh();
    }

    private function logEvent(Team $team, bool $isCorrect)
    {
        if ($isCorrect) {
            $eventName = EventList::EVENT_CORRECT;
            $message = trans('event.correct.power_up');
        } else {
            $eventName = EventList::EVENT_WRONG;
            $message = trans('event.wrong.power_up');
        }

        EventHistory::query()->create([
            'event_name' => $eventName,
            'team_name' => $team->team_name,
            'event' => $message
        ]);
    }

    private function getTeamPowerUp(Team $team): TeamPowerUp
    {
        return $team
            ->powerUps()
            ->where('power_name', $this->object->power_name)
            ->first();
    }
}
