<?php

namespace App\Services\Object;

use App\Helper\Event\EventList;
use App\Helper\Power\PowerList;
use App\Models\Event\EventHistory;
use App\Models\Object\ObjectDifference;
use App\Models\Team\Team;
use App\Models\Team\TeamPowerUp;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ObjectDifferenceService extends BaseObjectService
{
    /**
     * @var double $score
     */
    protected $score;

    /**
     * @var int $multiplier
     */
    protected $multiplier;

    public function __construct($objectId)
    {
        parent::__construct($objectId, new ObjectDifference());
        $this->multiplier = 1;
    }

    public function handleCorrect(Team $team): array
    {
        try {
            DB::beginTransaction();

            if ($this->shouldUsePower($team, PowerList::POWER_DOUBLE_SCORE)) {
                $this->multiplier = 2;
                $this->decrementPowerAmount($team, PowerList::POWER_DOUBLE_SCORE);
                $this->updateTeamScore($team, true);
                $this->logEvent($team, true, PowerList::POWER_DOUBLE_SCORE);

                $additionalMessage = "(Double Score digunakan)";
            } else {
                $team = $this->updateTeamScore($team, true);
                $this->logEvent($team, true, null);

                $additionalMessage = null;
            }

            $this->markAsGuessed($team);
            $response = [
                'status' => 1,
                'message' => "Tebakan benar $additionalMessage",
                'data' => [
                    'score' => $this->score,
                    'founder' => $team->team_name,
                    'total_score' => $team->statistic->score,
                ]
            ];

            DB::commit();
        } catch (\Exception $e) {
            Log::error($e->getMessage(), [$e]);
            DB::rollBack();
            abort(500);
        }

        return $response;
    }

    public function handleWrong(Team $team): array
    {
        try {
            DB::beginTransaction();

            if ($this->shouldUsePower($team, PowerList::POWER_NO_PENALTY)) {
                $this->multiplier = 0;
                $team = $this->updateTeamScore($team, false);
                $this->decrementPowerAmount($team, PowerList::POWER_NO_PENALTY);
                $this->logEvent($team, false, PowerList::POWER_NO_PENALTY);

                $additionalMessage = '(No Penalty digunakan)';
            } else {
                $team = $this->updateTeamScore($team, false);
                $this->logEvent($team, false, null);

                $additionalMessage = null;
            }

            $response = [
                'status' => 0,
                'message' => "Tebakan salah $additionalMessage",
                'data' => [
                    'score' => $this->score,
                    'total_score' => $team->statistic->score,
                ]
            ];

            DB::commit();
        } catch (\Exception $e) {
            Log::error($e->getMessage(), [$e]);
            DB::rollBack();
            abort(500);
        }

        return $response;
    }

    protected function logEvent(Team $team, bool $isCorrect, ?string $powerUpName)
    {
        if ($isCorrect) {
            $eventName = EventList::EVENT_CORRECT;

            if (is_null($powerUpName)) {
                $message = trans('event.correct.object_diff', ['SCORE' => $this->score]);
            } else {
                $message = trans("event.use.power_up.$powerUpName", ['SCORE' => $this->score]);
            }
        } else {
            $eventName = EventList::EVENT_WRONG;

            if (is_null($powerUpName)) {
                $message = trans('event.wrong.object_diff', ['SCORE' => $this->score]);
            } else {
                $message = trans("event.use.power_up.$powerUpName");
            }
        }

        EventHistory::query()->create([
            'event_name' => $eventName,
            'team_name' => $team->team_name,
            'event' => $message
        ]);
    }

    private function updateTeamScore(Team $team, $isCorrect)
    {
        $this->score = ($isCorrect)
            ? $this->object->score
            : $this->object->score ?? 1 * -1;

        $this->score *= $this->multiplier;

        $team->statistic->increment('score', $this->score);

        return $team->refresh();
    }

    private function shouldUsePower(Team $team, string $powerName): bool
    {
        $power = $team
            ->powerUps()
            ->where('power_name', $powerName)
            ->first();

        return isset($power) && $power->amount > 0;
    }

    private function decrementPowerAmount(Team $team, string $powerName): void
    {
        $team
            ->powerUps()
            ->where('power_name', $powerName)
            ->decrement('amount', 1);
    }
}
