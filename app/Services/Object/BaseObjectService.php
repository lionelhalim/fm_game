<?php

namespace App\Services\Object;

use App\Models\Object\ObjectDifference;
use App\Models\Object\ObjectModel;
use App\Models\Object\ObjectPowerUp;
use App\Models\Team\Team;

abstract class BaseObjectService
{
    /**
     * @var ?ObjectModel $object
     */
    public $object;

    /**
     * @param int $objectId
     * @param ObjectDifference | ObjectPowerUp $objectModel
     */
    public function __construct(int $objectId, $objectModel)
    {
        $this->object = $objectModel::query()->find($objectId);
    }

    abstract public function handleCorrect(Team $team): array;
    abstract public function handleWrong(Team $team): array;

    public function isGuessed(): bool
    {
        return $this->object->status ?? false;
    }

    protected function markAsGuessed(Team $team): void
    {
        $this->object->update([
            'status' => true,
            'founder' => $team->team_name
        ]);
        $this->object->refresh();
    }
}
