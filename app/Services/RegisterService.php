<?php

namespace App\Services;

use App\Models\Team\Team;
use App\Models\Team\TeamStatistic;
use Illuminate\Support\Facades\DB;

class RegisterService
{
    protected $team;
    protected $teamName;
    protected $password;

    public function __construct($teamName, $password)
    {
        $this->teamName = $teamName;
        $this->password = $password;
    }

    public function handle()
    {
        try {
            DB::beginTransaction();
            $this->createTeam();
            $this->createTeamStatistic();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            dd('exeption register => ' . $e->getMessage());
        }
    }

    public function createTeam(): void
    {
        $this->team = Team::query()->create([
            'team_name' => $this->teamName,
            'password' => $this->password,
        ]);
    }

    public function createTeamStatistic(): void
    {
        TeamStatistic::query()->create([
            'team_name' => $this->team->team_name,
            'score' => 0,
        ]);
    }
}
