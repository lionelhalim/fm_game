<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableEventHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event__history', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->string('event_name', 64);
            $table->string('team_name', 32);
            $table->text('event');
            $table->timestamps();

            $table->foreign('event_name')->references('event_name')->on('event');
            $table->foreign('team_name')->references('team_name')->on('team');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event__history');
    }
}
