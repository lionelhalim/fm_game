<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableObjectPowerUp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('object__power_up', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->string('object_type', 64);
            $table->string('object_name', 256);
            $table->string('power_name', 64);
            $table->text('question');
            $table->string('keyword', 256);
            $table->boolean('status')->default(false);
            $table->integer('amount')->default(1);
            $table->string('founder', 32)->nullable();

            $table->foreign('object_type')->references('object_type')->on('object');
            $table->foreign('power_name')->references('power_name')->on('power_up');
            $table->foreign('founder')->references('team_name')->on('team');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('object__power_up');
    }
}
