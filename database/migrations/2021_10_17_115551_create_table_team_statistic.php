<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTeamStatistic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team__statistic', function (Blueprint $table) {
            $table->string('team_name', 32)->primary()->index();
            $table->double('score');
            $table->timestamps();

            //$table->foreign('team_name')->references('team_name')->on('team');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team__statistic');
    }
}
