<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTeamPowerUp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team__power_up', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->string('power_name', 64);
            $table->string('team_name', 32);
            $table->integer('amount')->default(0);

            $table->foreign('power_name')->references('power_name')->on('power_up');
            $table->foreign('team_name')->references('team_name')->on('team');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team__power_up');
    }
}
