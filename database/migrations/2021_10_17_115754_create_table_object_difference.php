<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableObjectDifference extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('object__difference', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->string('object_type', 64);
            $table->string('object_name', 256);
            $table->boolean('status')->default(false);
            $table->double('score')->default(3);
            $table->string('founder', 32)->nullable();

            $table->foreign('object_type')->references('object_type')->on('object');
            $table->foreign('founder')->references('team_name')->on('team');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('object__difference');
    }
}
