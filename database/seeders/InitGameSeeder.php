<?php

namespace Database\Seeders;

use App\Helper\Event\EventList;
use App\Helper\Object\ObjectList;
use App\Helper\Power\PowerList;
use App\Helper\Team\TeamList;
use App\Models\Event\EventModel;
use App\Models\Object\ObjectDifference;
use App\Models\Object\ObjectModel;
use App\Models\Object\ObjectPowerUp;
use App\Models\Power\PowerUpModel;
use App\Models\Team\Team;
use App\Models\Team\TeamPowerUp;
use App\Models\Team\TeamStatistic;
use Illuminate\Database\Seeder;

class InitGameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->initBase();

        $this->initTeam();
        $this->initTeamPower();

        $this->initObjectDifference();
        $this->initObjectPowerUp();
    }

    public function initBase()
    {
        Team::query()->insert(TeamList::TEAMS);
        PowerUpModel::query()->insert(PowerList::POWERS);
        ObjectModel::query()->insert(ObjectList::OBJECTS);
        EventModel::query()->insert(EventList::EVENTS);
    }

    public function initTeam()
    {
        foreach (TeamList::TEAMS as $team) {
            TeamStatistic::query()->create([
                'team_name' => $team['team_name'],
                'score' => 0,
            ]);
        }
    }

    public function initTeamPower()
    {
        foreach (TeamList::TEAMS as $team) {
            foreach (PowerList::POWERS as $power) {
                $attributes = [
                    'power_name' => $power['power_name'],
                    'team_name' => $team['team_name'],
                    'amount' => 0,
                ];

                TeamPowerUp::query()->create($attributes);
            }
        }
    }

    public function initObjectDifference()
    {
        ObjectDifference::query()->insert(ObjectList::OBJECT_DIFFERENCE_LIST);
    }

    public function initObjectPowerUp()
    {
        ObjectPowerUp::query()->insert(ObjectList::OBJECT_POWER_UP_DOUBLE_SCORE);
        ObjectPowerUp::query()->insert(ObjectList::OBJECT_POWER_UP_NO_PENALTY);
    }
}
